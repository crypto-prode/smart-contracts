pragma solidity 0.8.13;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract ERC20Mock is ERC20, Ownable {
    constructor(string memory name, string memory symbol) ERC20(name, symbol) {}

    function mint(uint256 amount) external payable onlyOwner {
        _mint(msg.sender, amount);
    }

    function burn(uint256 amount) external payable {
        _burn(msg.sender, amount);
    }
}
