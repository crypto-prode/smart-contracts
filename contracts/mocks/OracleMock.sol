pragma solidity 0.8.13;

contract OracleMock {
    bool[] private _firstRoundResult = new bool[](8);
    bool[] private _secondRoundResult = new bool[](4);
    bool[] private _thirdRoundResult = new bool[](2);
    bool private _finalRoundResult;

    function getEighthsRoundResult() external view returns (bool[] memory) {
        return _firstRoundResult;
    }

    function getFourthRoundResult() external view returns (bool[] memory) {
        return _secondRoundResult;
    }

    function getSemifinalRoundResult() external view returns (bool[] memory) {
        return _thirdRoundResult;
    }

    function getFinalRoundResult() external view returns (bool) {
        return _finalRoundResult;
    }

    function setEighthsRoundResult(bool[] memory result) external {
        require(result.length == 8, "its an incorrect value");
        _firstRoundResult = result;
    }

    function setFourthRoundResult(bool[] memory result) external {
        require(result.length == 4, "its an incorrect value");
        _secondRoundResult = result;
    }

    function setSemifinalRoundResult(bool[] memory result) external {
        require(result.length == 2, "its an incorrect value");
        _thirdRoundResult = result;
    }

    function setFinalRoundResult(bool result) external {
        _finalRoundResult = result;
    }
}
