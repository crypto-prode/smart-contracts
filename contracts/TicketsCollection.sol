// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract TicketsCollection is ERC1155Supply, Ownable {
    uint256 public tokenId;
    string[] private _hashIpfs;

    address public _poolContract;
    mapping(uint256 => address) private nftOwners;
    mapping(uint256 => uint16) private teamNFT;
    mapping(address => uint256[]) private addressNft;

    struct Team {
        uint256 idNft;
        string hash;
    }

    constructor(string[] memory hashes) ERC1155("https://ipfs.io/ipfs/{id}") {
        _hashIpfs = hashes;
    }

    function mint(address minter, uint16 teamId) external returns (uint256) {
        onlyPool();
        onlyCorrectId(teamId);
        _mint(minter, teamId, 1, "");
        ++tokenId;
        nftOwners[tokenId] = minter;
        teamNFT[tokenId] = teamId;
        addressNft[minter].push(tokenId);

        return tokenId;
    }

    function setPool(address pool) external onlyOwner {
        require(_poolContract == address(0), "it's already set");
        require(
            pool != address(this) && pool != address(0),
            "it's a incorrect address"
        );
        _poolContract = pool;
    }

    function uri(uint256 id) public view override returns (string memory) {
        return string(abi.encodePacked("https://ipfs.io/ipfs/", _hashIpfs[id]));
    }

    function onlyPool() private view {
        require(msg.sender == _poolContract, "only Pool");
    }

    function onlyCorrectId(uint16 teamId) private view {
        require(_hashIpfs.length > teamId, "incorrect id");
    }

    function getOwnerNftId(uint256 nftId) external view returns (address) {
        return nftOwners[nftId];
    }

    function getNFTs(address _owner)
        external
        view
        returns (Team[] memory teams)
    {
        uint256[] memory nfts = addressNft[_owner];
        teams = new Team[](nfts.length);
        for (uint256 i; i < nfts.length; ++i) {
            uint256 nftId = nfts[i];
            teams[i] = Team(nftId, _hashIpfs[teamNFT[nftId]]);
        }
    }
}
