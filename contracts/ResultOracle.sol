// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

import "@openzeppelin/contracts/access/Ownable.sol";

contract ResultOracle is Ownable {
    address[] private _judges = new address[](3);
    uint256 private constant NUMBER_OF_JUDGES = 3;

    bool[] private _eighthsRoundResult = new bool[](8);
    bool[] private _fourthRoundResult = new bool[](4);
    bool[] private _semifinalRoundResult = new bool[](2);
    bool private _finalRoundResult;

    mapping(address => bool[]) public _eighthsRoundDebate;
    mapping(address => bool[]) public _fourthRoundDebate;
    mapping(address => bool[]) public _semifinalRoundDebate;
    mapping(address => bool) public _finalRoundDebate;

    mapping(address => bool) private _eighthsRoundVoted;
    mapping(address => bool) private _fourthRoundVoted;
    mapping(address => bool) private _semifinalRoundVoted;
    mapping(address => bool) private _finalRoundVoted;

    bool private _eighthsRoundDefined;
    bool private _fourthRoundDefined;
    bool private _semifinalRoundDefined;
    bool private _finalRoundDefined;

    constructor(address[] memory judges) {
        require(judges.length == NUMBER_OF_JUDGES, "you must set 3 judges");

        for (uint8 i; i < judges.length; ++i) {
            require(judges[i] != address(0), "a judge cant be address zero");
            require(judges[i] != msg.sender, "the owner cant be judge");
            require(
                !isJudge(judges[i]),
                "you can't set the same address twice as a judge"
            );
            _judges[i] = judges[i];

            _eighthsRoundDebate[judges[i]] = new bool[](8);
            _fourthRoundDebate[judges[i]] = new bool[](4);
            _semifinalRoundDebate[judges[i]] = new bool[](2);
        }
    }

    function getEighthsRoundResult() external view returns (bool[] memory) {
        require(_eighthsRoundDefined, "There is no result yet");
        return _eighthsRoundResult;
    }

    function getFourthRoundResult() external view returns (bool[] memory) {
        require(_fourthRoundDefined, "There is no result yet");
        return _fourthRoundResult;
    }

    function getSemifinalRoundResult() external view returns (bool[] memory) {
        require(_semifinalRoundDefined, "There is no result yet");
        return _semifinalRoundResult;
    }

    function getFinalRoundResult() external view returns (bool) {
        require(_finalRoundDefined, "There is no result yet");
        return _finalRoundResult;
    }

    function getJudge(uint256 index) external view returns (address) {
        require(index < NUMBER_OF_JUDGES, "Incorrect index");
        return _judges[index];
    }

    function setNewJudge(uint8 index, address newJudge) external onlyOwner {
        require(index < NUMBER_OF_JUDGES, "Incorrect index");
        require(newJudge != address(0), "a judge cant be address zero");
        require(newJudge != msg.sender, "the owner cant be judge");
        require(
            !isJudge(newJudge),
            "you can't set the same address twice as a judge"
        );
        _judges[index] = newJudge;
    }

    //add judge debate
    function addJudgeDebateToEighthsRound(bool[] memory vote) external {
        require(vote.length == 8, "its an incorrect value");
        require(isJudge(msg.sender), "you are not a judge");
        _eighthsRoundDebate[msg.sender] = vote;
        _eighthsRoundVoted[msg.sender] = true;
    }

    function addJudgeDebateToFourthRound(bool[] memory vote) external {
        require(vote.length == 4, "its an incorrect value");
        require(isJudge(msg.sender), "you are not a judge");
        _fourthRoundDebate[msg.sender] = vote;
        _fourthRoundVoted[msg.sender] = true;
    }

    function addJudgeDebateToSemifinalRound(bool[] memory vote) external {
        require(vote.length == 2, "its an incorrect value");
        require(isJudge(msg.sender), "you are not a judge");
        _semifinalRoundDebate[msg.sender] = vote;
        _semifinalRoundVoted[msg.sender] = true;
    }

    function addJudgeDebateToFinalRound(bool vote) external {
        require(isJudge(msg.sender), "you are not a judge");
        _finalRoundDebate[msg.sender] = vote;
        _finalRoundVoted[msg.sender] = true;
    }

    //set final vote
    function setEighthsRoundResult(bool[] memory vote) external onlyOwner {
        require(vote.length == 8, "its an incorrect value");
        require(
            isJudgesAddedEighthsRoundResults(),
            "the judges did not upload eighths round results"
        );
        require(!_eighthsRoundDefined, "a final vote has already been reached");
        for (uint256 i; i < vote.length; ) {
            uint8 flat;
            if (_eighthsRoundDebate[_judges[0]][i]) ++flat;
            if (_eighthsRoundDebate[_judges[1]][i]) ++flat;
            if (_eighthsRoundDebate[_judges[2]][i]) ++flat;
            if (vote[i]) ++flat;
            if (flat == 4 || flat == 3) {
                _eighthsRoundResult[i] = true;
            } else if (flat == 0 || flat == 1) {
                _eighthsRoundResult[i] = false;
            } else {
                revert("there is no match");
            }
            unchecked {
                ++i;
            }
        }
        _eighthsRoundDefined = true;
    }

    function isJudgesAddedEighthsRoundResults() private view returns (bool) {
        for (uint8 i; i < _judges.length; ++i) {
            if (!_eighthsRoundVoted[_judges[i]]) {
                return false;
            }
        }
        return true;
    }

    function setFourthRoundResult(bool[] memory vote) external onlyOwner {
        require(vote.length == 4, "its an incorrect value");
        require(
            isJudgesAddedFourthRoundResults(),
            "the judges did not upload fourth round results"
        );
        require(!_fourthRoundDefined, "a final vote has already been reached");
        for (uint256 i; i < vote.length; ) {
            uint8 flat;
            if (_fourthRoundDebate[_judges[0]][i]) ++flat;
            if (_fourthRoundDebate[_judges[1]][i]) ++flat;
            if (_fourthRoundDebate[_judges[2]][i]) ++flat;
            if (vote[i]) ++flat;
            if (flat == 4 || flat == 3) {
                _fourthRoundResult[i] = true;
            } else if (flat == 0 || flat == 1) {
                _fourthRoundResult[i] = false;
            } else {
                revert("there is no match");
            }
            unchecked {
                ++i;
            }
        }
        _fourthRoundDefined = true;
    }

    function isJudgesAddedFourthRoundResults() private view returns (bool) {
        for (uint8 i; i < _judges.length; ++i) {
            if (!_fourthRoundVoted[_judges[i]]) {
                return false;
            }
        }
        return true;
    }

    function setSemifinalRoundResult(bool[] memory vote) external onlyOwner {
        require(vote.length == 2, "its an incorrect value");
        require(
            isJudgesAddedSemifinalRoundResults(),
            "the judges did not upload semifinal round results"
        );
        require(
            !_semifinalRoundDefined,
            "a final vote has already been reached"
        );
        for (uint256 i; i < vote.length; ++i) {
            uint8 flat;
            if (_semifinalRoundDebate[_judges[0]][i]) ++flat;
            if (_semifinalRoundDebate[_judges[1]][i]) ++flat;
            if (_semifinalRoundDebate[_judges[2]][i]) ++flat;
            if (vote[i]) ++flat;
            if (flat == 4 || flat == 3) {
                _semifinalRoundResult[i] = true;
            } else if (flat == 0 || flat == 1) {
                _semifinalRoundResult[i] = false;
            } else {
                revert("there is no match");
            }
        }
        _semifinalRoundDefined = true;
    }

    function isJudgesAddedSemifinalRoundResults() private view returns (bool) {
        for (uint8 i; i < _judges.length; ++i) {
            if (!_semifinalRoundVoted[_judges[i]]) {
                return false;
            }
        }
        return true;
    }

    function setFinalRoundResult(bool vote) external onlyOwner {
        require(
            isJudgesAddedFinalRoundResults(),
            "the judges did not upload final round results"
        );
        require(!_finalRoundDefined, "a final vote has already been reached");
        uint8 flat;
        if (_finalRoundDebate[_judges[0]]) ++flat;
        if (_finalRoundDebate[_judges[1]]) ++flat;
        if (_finalRoundDebate[_judges[2]]) ++flat;
        if (vote) ++flat;

        if (flat == 4 || flat == 3) {
            _finalRoundResult = true;
        } else if (flat == 0 || flat == 1) {
            _finalRoundResult = false;
        } else {
            revert("there is no match");
        }
        _finalRoundDefined = true;
    }

    function isJudgesAddedFinalRoundResults() private view returns (bool) {
        for (uint8 i; i < _judges.length; ++i) {
            if (!_finalRoundVoted[_judges[i]]) {
                return false;
            }
        }
        return true;
    }

    function isJudge(address aspirant) private view returns (bool) {
        if (aspirant == _judges[0]) return true;
        if (aspirant == _judges[1]) return true;
        if (aspirant == _judges[2]) return true;
        return false;
    }

    function transferOwnership(address newOwner) public override onlyOwner {
        require(
            newOwner != address(0),
            "Ownable: new owner is the zero address"
        );
        require(!isJudge(newOwner), "Ownable: new owner is a judge address");

        _transferOwnership(newOwner);
    }
}
