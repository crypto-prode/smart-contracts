// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {
  //nfts ipfs
  //xeneizes(#1): Qmc5xu44Vefuzdq79Cr8aCohcWfJo8mfn7jUh2zDNxhtp4
  //tolimas: QmNjzLjPDCAj7mHm61p6nF31RmdQNSWWpMYG2YPgTnxPo4
  //sabalero: QmenQionoiTRPrKMdnfpvLQ9Vsc8dQKFzgMGGW3TLGsL5Z
  //pincharrata(#4): QmTAEuBcqqvUSrn36uCeuJzEunimZbdMKsz9BrRPTnMsGi
  //paranoensis: QmaVHhTp3gSegAVatyb7bmEaMP3guorWdzDu3mx5GCUoGa
  //palmeras: QmZC7RRWyNoFtxmGaff64vXyaUA4JK9PQeH1rsYD65fxGs
  //mineiros: QmeBpjsNHWx9KGd4Wxx5bMXKEkq7itSzzWKrTjbQ2ufn1b
  //millos(#8):  QmU1yEJwWbeMHvC6TbZJ1u3RnsKG8DH1ReksYCoRDJdE6M
  //matador: Qmatwk2zx1fT6yStBdRGZ5iovEZkq77tRDB5hS3skacR1v
  //libertades: Qmd8ocukdbgzPudFzLNA2JJyCHVkiqyjstDDxzHJDC3vTS 
  //fortin(#11): QmPmAXqidrwykGNFGvtugck14jThiwJ2kh3G8jnhsrzrnt
  //fortalece: QmNinT5b1vrDxLxvkGVqwW2CjW26ifpBdpo97pFX8hEa9s
  //flamenguistas(#13): QmYf3Rcrw6FmAXaTpwFXM3YH7QesL4Td7So7KRL7atjMdj
  //emelecs: QmR2NNMJBHhAMHfe7Mb9A5T1MWeNtt3dNiwyLiiMVG213H
  //corinthiano(#15): QmSNz8mvW9kPcPwUR8e1EbRE6pBsRn6h3KUeAWTeGEKL9s
  //cerro(#16): QmX3vgsRLzZexwTmSA11BLaLk4WqC7ZbWfeFj5eHmsGFNt

  const CryptoProdeCollection = await hre.ethers.getContractFactory("CryptoProdeCollection");
  const nftContract = await CryptoProdeCollection.deploy([]); //ipfs
  await nftContract.deployed();
  console.log("nftContract deployed to:", nftContract.address);

  const ResultOracle = await hre.ethers.getContractFactory("ResultOracle");
  const oracle = await ResultOracle.deploy([]); //judges addresses
  await oracle.deployed();
  console.log("oracle deployed to:", oracle.address);

  const Pool = await hre.ethers.getContractFactory("Pool");
  const pool = await Pool.deploy("0x8a9424745056Eb399FD19a0EC26A14316684e274", nftContract.address, oracle.address);
  await pool.deployed();//DAI contract, nftContract address, oracle address
  console.log("pool deployed to:", pool.address);

  await nftContract.setPool(pool.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
