
# Crypto Prode

  

El objetivo de este proyecto es realizar un pronostico deportivo (PRODE) sobre la Copa Libertador.

Mintearemos nft que toman el papel de tickets, de los cuales existirán 16 tipos distintos que representaran los 16 equipos que se encuentran jugando los octavos de final de la Copa Libertadores.

  

El minteo se realizara seleccionando un equipo a modo de cosmetico. La moneda de pago y los beneficios van a ser realizados en `DAI's`.

  

Al terminar el periodo de minteo, todo los tokens que se recolectaran irán a un pool. En ese pool, los owners de `NFT's` podrán seleccionar que equipos pasan a la próxima ronda (no se selecciona una cantidad de goles, solo que equipo pasa de ronda). La selección solo se realiza de la ronda actual, no de todo el torneo.

  

Si se cumple que:

  

**Octavos:** 5 o mas resultados correctos de 8 partidos -> `ganan.`

**Cuartos:** 3 o mas resultados correctos de 4 partidos -> `ganan`

**Semis:** 2 partidos con resultado correcto de 2 partidos -> `ganan`
  
**Final:** 1 partido con resultado correcto de 1 partidos -> `ganan`


Entre ronda y ronda, si jugaste la ronda anterior podras desde ese momento hasta cuando vos quieras retirar los rewards que generaste por acertar el resultado.

  

Los periodos van a ser los siguientes :

1)   `Compra/minteo de NFT's octavos`.

2) `Selección de decisión de octavos.`

3) `La espera de los resultados (periodo de cobro).`

4)  `Compra/minteo de NFT's cuartos`.

5) `Selección de decisión de cuartos.`

6) `La espera de los resultados (periodo de cobro).`

7)   `Compra/minteo de NFT's semis`.

8) `Selección de decisión de semis.`

9) `La espera de los resultados (periodo de cobro).`

10) `Selección de decisión de final.`

11) `Selección de decisión de final.`

12)  `La espera de los resultados (periodo de cobro).`



# Distribución del Pool
  
Cada ronda tendra su propio pool y repartira de la siguiente manera:

90% - Predicción deportiva (en cada ronda se repartirá el 90% de lo recolectado en esa ronda).
  
10% - Equipo de devs (costos de deploy, pagina web, sueldos, etc.) Este porcentaje lo recibirá el equipo al finalizar cada ronda del torneo.
