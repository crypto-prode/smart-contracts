const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("addJudgeDebateToEighthsRound()", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ResultOracle = await ethers.getContractFactory("ResultOracle", owner);
        this.resultOracle = await ResultOracle.deploy([user_3.address, user.address, user_2.address]);
    });

    describe("addJudgeDebateToEighthsRound() Function", function () {
        it("should revert when try to add debate with more than 8 results", async function () {
            const vote = [true, true, true, true, false, false, true, true, true];

            await expect(this.resultOracle.connect(user_2).addJudgeDebateToEighthsRound(vote)).to.be.revertedWith(
                "its an incorrect value");
        });

        it("should revert when try to add debate with a non judge", async function () {
            const vote = [true, true, true, true, false, false, true, true];

            await expect(this.resultOracle.connect(user_4).addJudgeDebateToEighthsRound(vote)).to.be.revertedWith(
                "you are not a judge");
        });

        it("should revert when try to set result with a incorrect vote", async function () {
            const vote = [true, false, false, true, true, false, false, true, false];

            await expect(this.resultOracle.setEighthsRoundResult(vote)).to.be.revertedWith(
                "its an incorrect value");
        });

        it("should revert when try to set result with not owner address", async function () {
            const vote = [true, false, false, true, false, false, true, false];

            await expect(this.resultOracle.connect(user).setEighthsRoundResult(vote)).to.be.revertedWith(
                "Ownable: caller is not the owner");
        });

        it("should revert when try to set result but there is not debates", async function () {
            const vote = [true, false, false, true, false, false, true, false];

            await expect(this.resultOracle.setEighthsRoundResult(vote)).to.be.revertedWith(
                "the judges did not upload eighths round results");
        });

        it("should revert when try to set result but there is not enough debates", async function () {
            const vote = [true, false, false, true, false, false, true, false];

            this.resultOracle.connect(user_2).addJudgeDebateToEighthsRound(vote);
            this.resultOracle.connect(user).addJudgeDebateToEighthsRound(vote);

            await expect(this.resultOracle.setEighthsRoundResult(vote)).to.be.revertedWith(
                "the judges did not upload eighths round results");
        });

        it("should set correct debate when you set with correct data", async function () {
            await ethers.provider.send("evm_setNextBlockTimestamp", [1657508401]);
            const vote = [true, true, true, true, false, false, true, true];

            await this.resultOracle.connect(user_3).addJudgeDebateToEighthsRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToEighthsRound(vote);
            await this.resultOracle.connect(user).addJudgeDebateToEighthsRound(vote);

            await this.resultOracle.setEighthsRoundResult(vote);

            expect(vote).to.eql(await this.resultOracle.connect(user).getEighthsRoundResult());
        });

        it("should revert if there is not match between 3 judges or more", async function () {
            const vote = [true, false, false, true, false, false, true, false];
            const vote1 = [false, false, false, true, false, false, true, false];


            await this.resultOracle.connect(user_3).addJudgeDebateToEighthsRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToEighthsRound(vote1);
            await this.resultOracle.connect(user).addJudgeDebateToEighthsRound(vote1);

            await expect(this.resultOracle.setEighthsRoundResult(vote)).to.be.revertedWith(
                "there is no match");
        });

        it("should set if there are just one vote different", async function () {
            const vote = [true, false, false, true, false, false, true, false];
            const vote1 = [false, false, false, true, false, false, true, false];

            await this.resultOracle.connect(user_3).addJudgeDebateToEighthsRound(vote1);
            await this.resultOracle.connect(user_2).addJudgeDebateToEighthsRound(vote1);
            await this.resultOracle.connect(user).addJudgeDebateToEighthsRound(vote1);

            await this.resultOracle.setEighthsRoundResult(vote);

            const result = await this.resultOracle.getEighthsRoundResult();

            expect(vote1).to.eql(result);
        });

        it("should revert when try to set with a previous consensus", async function () {
            const vote = [true, false, false, true, false, false, true, false];
            const vote1 = [false, false, false, true, false, false, true, false];

            await this.resultOracle.connect(user_3).addJudgeDebateToEighthsRound(vote1);
            await this.resultOracle.connect(user_2).addJudgeDebateToEighthsRound(vote1);
            await this.resultOracle.connect(user).addJudgeDebateToEighthsRound(vote1);

            await this.resultOracle.setEighthsRoundResult(vote);

            await expect(this.resultOracle.setEighthsRoundResult(vote1)).to.be.revertedWith(
                "a final vote has already been reached");
        });

        it("should revert if there is 2 match an 2 match in one result", async function () {
            const vote = [true, false, false, true, false, false, true, false];
            const vote1 = [false, false, false, true, false, false, true, false];
            const vote2 = [true, false, false, true, false, false, true, false];

            await this.resultOracle.connect(user_3).addJudgeDebateToEighthsRound(vote1);
            await this.resultOracle.connect(user_2).addJudgeDebateToEighthsRound(vote1);
            await this.resultOracle.connect(user).addJudgeDebateToEighthsRound(vote2);

            await expect(this.resultOracle.setEighthsRoundResult(vote)).to.be.revertedWith(
                "there is no match");
        });
    });
});