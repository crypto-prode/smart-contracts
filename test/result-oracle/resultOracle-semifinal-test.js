const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("addJudgeDebateToSemifinalRound()", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ResultOracle = await ethers.getContractFactory("ResultOracle", owner);
        this.resultOracle = await ResultOracle.deploy([user_3.address, user.address, user_2.address]);
    });

    describe("addJudgeDebateToSemifinalRound() Function", function () {
        it("should revert when try to add debate with a incorrect vote", async function () {
            const vote = [true, false, false, true, true, false, false, true];

            await expect(this.resultOracle.connect(user_2).addJudgeDebateToSemifinalRound(vote)).to.be.revertedWith(
                "its an incorrect value");
        });

        it("should revert when try to add debate with a non judge", async function () {
            const vote = [true, false];

            await expect(this.resultOracle.connect(user_4).addJudgeDebateToSemifinalRound(vote)).to.be.revertedWith(
                "you are not a judge");
        });

        it("should revert when try to set result with a incorrect vote", async function () {
            const vote = [true, true, true, true];

            await expect(this.resultOracle.setSemifinalRoundResult(vote)).to.be.revertedWith(
                "its an incorrect value");
        });

        it("should revert when try to set result with not owner address", async function () {
            const vote = [true, false];

            await expect(this.resultOracle.connect(user).setSemifinalRoundResult(vote)).to.be.revertedWith(
                "Ownable: caller is not the owner");
        });

        it("should set correct debate when you set with correct data", async function () {
            await ethers.provider.send("evm_setNextBlockTimestamp", [1662778804]);

            const vote = [true, false];

            await this.resultOracle.connect(user_3).addJudgeDebateToSemifinalRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToSemifinalRound(vote);
            await this.resultOracle.connect(user).addJudgeDebateToSemifinalRound(vote);

            await this.resultOracle.setSemifinalRoundResult(vote);

            expect(vote).to.eql(await this.resultOracle.connect(user).getSemifinalRoundResult());
        });

        it("should revert when try to set result but is before the time correct to vote", async function () {
            const vote = [true, true];

            await expect(this.resultOracle.setSemifinalRoundResult(vote)).to.be.revertedWith(
                "the judges did not upload semifinal round results");
        });

        it("should revert when try to set result but there is not enough debates", async function () {
            const vote = [true, true];

            this.resultOracle.connect(user).addJudgeDebateToSemifinalRound(vote);
            this.resultOracle.connect(user_2).addJudgeDebateToSemifinalRound(vote);
            await expect(this.resultOracle.setSemifinalRoundResult(vote)).to.be.revertedWith(
                "the judges did not upload semifinal round results");
        });

        it("should revert when there is no match in one result", async function () {
            const vote = [false, true];
            const vote1 = [true, true];

            await this.resultOracle.connect(user_3).addJudgeDebateToSemifinalRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToSemifinalRound(vote1);
            await this.resultOracle.connect(user).addJudgeDebateToSemifinalRound(vote1);

            await expect(this.resultOracle.setSemifinalRoundResult(vote)).to.be.revertedWith(
                "there is no match");
        });

        it("should return correct result when there is one judge with a different vote", async function () {
            const vote = [true, false];
            const vote1 = [false, true];

            await this.resultOracle.connect(user_3).addJudgeDebateToSemifinalRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToSemifinalRound(vote);
            await this.resultOracle.connect(user).addJudgeDebateToSemifinalRound(vote1);

            await this.resultOracle.setSemifinalRoundResult(vote);

            const result = await this.resultOracle.getSemifinalRoundResult();

            expect(vote).to.eql(result);
        });


        it("should revert when try to set with a previous consensus", async function () {
            const vote = [true, false];
            const vote1 = [false, true];

            await this.resultOracle.connect(user_3).addJudgeDebateToSemifinalRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToSemifinalRound(vote);
            await this.resultOracle.connect(user).addJudgeDebateToSemifinalRound(vote1);

            await this.resultOracle.setSemifinalRoundResult(vote);

            await expect(this.resultOracle.setSemifinalRoundResult(vote1)).to.be.revertedWith(
                "a final vote has already been reached");
        });
    });
});