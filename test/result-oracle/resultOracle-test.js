const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Result Oracle", function () {
  let owner, user, user_2, user_3, user_4;

  beforeEach(async function () {
    [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

    const ResultOracle = await ethers.getContractFactory("ResultOracle", owner);
    this.resultOracle = await ResultOracle.deploy([user_3.address, user.address, user_2.address]);
  });

  describe("constructor() Function", function () {
    it("should revert when try to set the owner as jusge in eighths position", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([owner.address, user.address, user_2.address])).to.be.revertedWith("the owner cant be judge");
    });

    it("should revert when try to set the owner as jusge in semifinal position", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([user.address, user_2.address, owner.address])).to.be.revertedWith("the owner cant be judge");
    });

    it("should revert when try to set more than 3 address as judge", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([user.address, user_2.address, user_3.address, owner.address])).to.be.revertedWith("you must set 3 judges");
    });

    it("should revert when try to set address zero as judge", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([user.address, user_2.address, ethers.constants.AddressZero])).to.be.revertedWith("a judge cant be address zero");
    });

    it("should revert when try to set same address as judge", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([user.address, user_2.address, user_2.address])).to.be.revertedWith("you can't set the same address twice as a judge");
    });

    it("should getJudge() function to give the address with the same order as saved in the constructor", async function () {
      expect(user_3.address).to.eq(await this.resultOracle.getJudge(0));
      expect(user.address).to.eq(await this.resultOracle.getJudge(1));
      expect(user_2.address).to.eq(await this.resultOracle.getJudge(2));
    });
  });

  describe("setNewJudge() Function", function () {
    it("should can set a correct index with a non used judge", async function () {
      await this.resultOracle.setNewJudge(0, user_4.address);

      expect(user_4.address).to.eq(await this.resultOracle.getJudge(0));
    });

    it("set jugde with not owner address", async function () {
      await expect(this.resultOracle.connect(user).setNewJudge(0, user_2.address)).to.be.revertedWith(
        "Ownable: caller is not the owner");
    });

    it("should revert when thy to set again an existent judge", async function () {
      await expect(this.resultOracle.setNewJudge(0, user_2.address)).to.be.revertedWith(
        "you can't set the same address twice as a judge");
    });

    it("should revert when try to set the owner as judge", async function () {
      await expect(this.resultOracle.setNewJudge(0, owner.address)).to.be.revertedWith(
        "the owner cant be judge");
    });

    it("should revert when try to set address(0) as judge", async function () {
      await expect(this.resultOracle.setNewJudge(0, ethers.constants.AddressZero)).to.be.revertedWith(
        "a judge cant be address zero");
    });

    it("should revert when index is incorrect", async function () {
      await expect(this.resultOracle.setNewJudge(5, user_4.address)).to.be.revertedWith(
        "Incorrect index");
    });

    it("should revert when index is incorrect in getJudge", async function () {
      await this.resultOracle.setNewJudge(0, user_4.address);

      await expect(this.resultOracle.getJudge(5)).to.be.revertedWith(
        "Incorrect index");
    });
  });
});

describe("getEighthsRoundResult", function () {
  let owner, user, user_2, user_3, user_4;

  beforeEach(async function () {
    [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

    const ResultOracle = await ethers.getContractFactory("ResultOracle", owner);
    this.resultOracle = await ResultOracle.deploy([user_3.address, user.address, user_2.address]);

  });

  describe("getEighthsRoundResult() Function", function () {
    it("should revert when there is no result", async function () {
      await expect(this.resultOracle.getEighthsRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return the same result that everyone vote", async function () {
      await ethers.provider.send("evm_setNextBlockTimestamp", [1657508800]);

      const vote = [true, true, true, true, false, false, true, true];

      await this.resultOracle.connect(user).addJudgeDebateToEighthsRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToEighthsRound(vote);
      await this.resultOracle.connect(user_3).addJudgeDebateToEighthsRound(vote);

      await this.resultOracle.setEighthsRoundResult(vote);

      expect(vote).to.eql(await this.resultOracle.connect(user).getEighthsRoundResult());

    });

    it("should revert when try to vote with a non judge", async function () {
      const vote = [true, true, true, true, false, false, true, true];

      await this.resultOracle.connect(user).addJudgeDebateToEighthsRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToEighthsRound(vote);
      await expect(this.resultOracle.connect(user_4).addJudgeDebateToEighthsRound(vote)).to.be.revertedWith(
        "you are not a judge");
    });
  });

});


describe("getFourthRoundResult()", function () {
  let owner, user, user_2, user_3, user_4;

  beforeEach(async function () {
    [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

    const ResultOracle = await ethers.getContractFactory("ResultOracle", owner);
    this.resultOracle = await ResultOracle.deploy([user_3.address, user.address, user_2.address]);
  });

  describe("getFourthRoundResult() Function", function () {
    it("should revert when there is no result", async function () {
      await expect(this.resultOracle.getFourthRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return the same result that everyone vote", async function () {
      await ethers.provider.send("evm_setNextBlockTimestamp", [1660532401]);

      const vote = [true, false, false, true];

      await this.resultOracle.connect(user).addJudgeDebateToFourthRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToFourthRound(vote);
      await this.resultOracle.connect(user_3).addJudgeDebateToFourthRound(vote);

      await this.resultOracle.setFourthRoundResult(vote);

      expect(vote).to.eql(await this.resultOracle.connect(user).getFourthRoundResult());
    });
  });

});


describe("getSemifinalRoundResult()", function () {
  let owner, user, user_2, user_3, user_4;

  beforeEach(async function () {
    [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

    const ResultOracle = await ethers.getContractFactory("ResultOracle", owner);
    this.resultOracle = await ResultOracle.deploy([user_3.address, user.address, user_2.address]);
  });

  describe("getSemifinalRoundResult() Function", function () {
    it("should revert when there is no result", async function () {
      await expect(this.resultOracle.getSemifinalRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return the same result that everyone vote", async function () {
      await ethers.provider.send("evm_setNextBlockTimestamp", [1662778800]);
      const vote = [true, false];

      await this.resultOracle.connect(user).addJudgeDebateToSemifinalRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToSemifinalRound(vote);
      await this.resultOracle.connect(user_3).addJudgeDebateToSemifinalRound(vote);

      await this.resultOracle.setSemifinalRoundResult(vote);

      expect(vote).to.eql(await this.resultOracle.connect(user).getSemifinalRoundResult());
    });
  });
});


describe("getFinalRoundResult()", function () {
  let owner, user, user_2, user_3, user_4;

  beforeEach(async function () {
    [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

    const ResultOracle = await ethers.getContractFactory("ResultOracle", owner);
    this.resultOracle = await ResultOracle.deploy([user_3.address, user.address, user_2.address]);
  });

  describe("getFinalRoundResult() Function", function () {
    it("should revert when there is no result", async function () {
      await expect(this.resultOracle.getFinalRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return the same result that everyone vote", async function () {
      await ethers.provider.send("evm_setNextBlockTimestamp", [1667271605]);
      const vote = true;

      await this.resultOracle.connect(user).addJudgeDebateToFinalRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToFinalRound(vote);
      await this.resultOracle.connect(user_3).addJudgeDebateToFinalRound(vote);

      await this.resultOracle.setFinalRoundResult(vote);

      expect(vote).to.eql(await this.resultOracle.connect(user).getFinalRoundResult());
    });
  });
});
