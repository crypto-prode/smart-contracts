const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("addJudgeDebateToFinalRound()", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ResultOracle = await ethers.getContractFactory("ResultOracle", owner);
        this.resultOracle = await ResultOracle.deploy([user_3.address, user.address, user_2.address]);
    });

    describe("addJudgeDebateToFinalRound() Function", function () {
        it("should revert when try to add debate with a non judge", async function () {
            const vote = [true, false];

            await expect(this.resultOracle.connect(user_4).addJudgeDebateToFinalRound(vote)).to.be.revertedWith(
                "you are not a judge");
        });

        it("should revert when try to set result but there is not debates", async function () {
            const vote = true;

            await expect(this.resultOracle.setFinalRoundResult(vote)).to.be.revertedWith(
                "the judges did not upload final round results");
        });

        it("should revert when try to set result with not owner address", async function () {
            const vote = true;

            await expect(this.resultOracle.connect(user).setFinalRoundResult(vote)).to.be.revertedWith(
                "Ownable: caller is not the owner");
        });

        it("should set correct debate when you set with correct data", async function () {
            await ethers.provider.send("evm_setNextBlockTimestamp", [1667271650]);

            const vote = true;

            await this.resultOracle.connect(user_3).addJudgeDebateToFinalRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToFinalRound(vote);
            await this.resultOracle.connect(user).addJudgeDebateToFinalRound(vote);

            await this.resultOracle.setFinalRoundResult(vote);

            expect(vote).to.eql(await this.resultOracle.connect(user).getFinalRoundResult());
        });

        it("should revert when there is no match between the judges", async function () {
            const vote = true;

            await this.resultOracle.connect(user_3).addJudgeDebateToFinalRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToFinalRound(!vote);
            await this.resultOracle.connect(user).addJudgeDebateToFinalRound(!vote);

            await expect(this.resultOracle.setFinalRoundResult(vote)).to.be.revertedWith(
                "there is no match");
        });

        it("should set correct debate when you set with 3 vote of 4", async function () {
            const vote = true;

            await this.resultOracle.connect(user_3).addJudgeDebateToFinalRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToFinalRound(vote);
            await this.resultOracle.connect(user).addJudgeDebateToFinalRound(!vote);

            await this.resultOracle.setFinalRoundResult(vote);

            expect(vote).to.eql(await this.resultOracle.connect(user).getFinalRoundResult());
        });

        it("should revert when try to set with a previous consensus", async function () {
            const vote = true;

            await this.resultOracle.connect(user_3).addJudgeDebateToFinalRound(vote);
            await this.resultOracle.connect(user_2).addJudgeDebateToFinalRound(vote);
            await this.resultOracle.connect(user).addJudgeDebateToFinalRound(!vote);

            await this.resultOracle.setFinalRoundResult(vote);

            await expect(this.resultOracle.setFinalRoundResult(false)).to.be.revertedWith(
                "a final vote has already been reached");
        });
    });
});