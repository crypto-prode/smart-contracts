const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("TicketsCollection", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const TicketsCollection = await ethers.getContractFactory("TicketsCollection", owner);
        this.nftContract = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi",
            "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract2 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract3 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract4 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address, 1657594799, 1660618799, 1663037999, 1667617199);

        await this.stablecoin.mint(ethers.utils.parseEther("1000"));
    });

    describe("Functions", function () {
        it("Should revert when try to mint with any pool", async function () {
            await expect(this.nftContract.connect(user).mint(user.address, 1)).to.be.revertedWith("only Pool");
        });

        it("Should revert when try to mint with a non pool", async function () {
            await this.nftContract.setPool(user_2.address);
            await expect(this.nftContract.connect(user).mint(user.address, 1)).to.be.revertedWith("only Pool");
        });

        it("Should revert when try to set for second time", async function () {
            await this.nftContract.setPool(user_2.address);
            await expect(this.nftContract.setPool(user_3.address)).to.be.revertedWith("it's already set");
        });

        it("Should revert when try to set with address(this)", async function () {
            await expect(this.nftContract.setPool(this.nftContract.address)).to.be.revertedWith("it's a incorrect address");
        });

        it("Should revert when try to mint with a incorrect id", async function () {
            await this.nftContract.setPool(user_2.address);
            await expect(this.nftContract.connect(user_2).mint(user.address, 100)).to.be.revertedWith("incorrect id");
        });

        it("Should check one getNFTs()", async function () {
            await this.nftContract.setPool(user_2.address);
            await this.nftContract.connect(user_2).mint(user.address, 1);

            const user2NFTs = await this.nftContract.getNFTs(user.address);
            expect(1).to.eq(user2NFTs[0].idNft);
        });

        it("Should check one uri()", async function () {
            await this.nftContract.setPool(user_2.address);
            await this.nftContract.connect(user_2).mint(user.address, 1);

            const user2NFTs = await this.nftContract.getNFTs(user.address);
            const hashNFt = await this.nftContract.uri(1);
            expect(hashNFt).to.eq("https://ipfs.io/ipfs/" + user2NFTs[0].hash);
        });

        it("Should check three getNFTs()", async function () {
            await this.nftContract.setPool(user_2.address);
            await this.nftContract.connect(user_2).mint(user.address, 1);
            await this.nftContract.connect(user_2).mint(user_2.address, 3);
            await this.nftContract.connect(user_2).mint(user_3.address, 2);
            await this.nftContract.connect(user_2).mint(user.address, 8);
            await this.nftContract.connect(user_2).mint(user_2.address, 6);
            await this.nftContract.connect(user_2).mint(user.address, 2);


            const user2NFTs = await this.nftContract.getNFTs(user.address);
            expect(1).to.eq(user2NFTs[0].idNft);
            expect(4).to.eq(user2NFTs[1].idNft);
            expect(6).to.eq(user2NFTs[2].idNft);
        });

        it("Should check getOwnerNftId()", async function () {
            await this.nftContract.setPool(user_2.address);
            await this.nftContract.connect(user_2).mint(user.address, 1);
            await this.nftContract.connect(user_2).mint(user_2.address, 3);
            await this.nftContract.connect(user_2).mint(user_3.address, 2);
            await this.nftContract.connect(user_2).mint(user.address, 8);
            await this.nftContract.connect(user_2).mint(user_2.address, 6);
            await this.nftContract.connect(user_2).mint(user.address, 2);

            const ownerNFTs3 = await this.nftContract.getOwnerNftId(3);
            expect(user_3.address).to.eq(ownerNFTs3);
        });

        it("Should check getOwnerNftId() with incorrect id", async function () {
            await this.nftContract.setPool(user_2.address);
            await this.nftContract.connect(user_2).mint(user.address, 1);
            await this.nftContract.connect(user_2).mint(user_2.address, 3);
            await this.nftContract.connect(user_2).mint(user_3.address, 2);
            await this.nftContract.connect(user_2).mint(user.address, 8);
            await this.nftContract.connect(user_2).mint(user_2.address, 6);
            await this.nftContract.connect(user_2).mint(user.address, 2);

            const ownerNFTsIdIncorrect = await this.nftContract.getOwnerNftId(10);
            expect(ethers.constants.AddressZero).to.eq(ownerNFTsIdIncorrect);
        });
    });
});