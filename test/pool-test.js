const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Pool", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const TicketsCollection = await ethers.getContractFactory("TicketsCollection", owner);
        this.nftContract = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi",
            "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract2 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract3 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract4 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address, 1657594799, 1660618799, 1663037999, 1667617199);

        await this.nftContract.setPool(this.pool.address);
        await this.nftContract2.setPool(this.pool.address);
        await this.nftContract3.setPool(this.pool.address);
        await this.nftContract4.setPool(this.pool.address);

        await this.stablecoin.mint(ethers.utils.parseEther("1000"));
    });

    describe("buyNFT() Function", function () {
        it("Should revert when try to buy nft with 0 DAI", async function () {
            await expect(this.pool.connect(user).buyNFT(1)).to.be.reverted;
        });

        it("Should revert there is no allowance", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("110"));

            await expect(this.pool.connect(user).buyNFT(1)).to.be.revertedWith("ERC20: insufficient allowance");
        });

        it("Should revert when try to buy a NFT with incorrect id in eighths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("110"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));

            await expect(this.pool.connect(user).buyNFT(16)).to.be.revertedWith("That id is non-existent");
        });

        it("Should revert when try to buy a NFT with incorrect id in fourth", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("110"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("30"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await expect(this.pool.connect(user).buyNFT(8)).to.be.revertedWith("That id is non-existent");
        });

        it("Should revert when try to buy a NFT with incorrect id in semifinal", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("110"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);


            await expect(this.pool.connect(user).buyNFT(4)).to.be.revertedWith("That id is non-existent");
        });

        it("Should revert when try to buy a NFT with incorrect id in final", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("110"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await expect(this.pool.connect(user).buyNFT(2)).to.be.revertedWith("That id is non-existent");
        });

        it("Should Eighths total prize increase when try to buy nft with 10 DAI ", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("110"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));

            await this.pool.connect(user).buyNFT(1);

            expect(ethers.utils.parseEther("10")).to.eq(await this.pool.getEighthsPoolPrize());
        });

        it("Should fourth total prize increase when try to buy nft with 30 DAI ", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("40"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("30"));

            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("40"));
            await this.pool.connect(user).buyNFT(1);

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("30"));
            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(1);

            expect(ethers.utils.parseEther("60")).to.eq(await this.pool.getFourthPoolPrize());
        });

        it("Should semis total prize increase when try to buy nft with 50 DAI ", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(1);

            expect(ethers.utils.parseEther("100")).to.eq(await this.pool.getSemifinalPoolPrize());
        });

        it("Should revert eighths when miss approve contract", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));

            await expect(this.pool.connect(user).buyNFT(4)).to.be.revertedWith("ERC20: insufficient allowance");
        });

        it("Should revert eighths when cant transfer DAIs to contract", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("5"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("15"));

            await expect(this.pool.connect(user).buyNFT(4)).to.be.revertedWith("ERC20: transfer amount exceeds balance");
        });

        it("Should revert fourth when miss approve contract", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await expect(this.pool.connect(user).buyNFT(4)).to.be.revertedWith("ERC20: insufficient allowance");
        });

        it("Should revert fourth when cant transfer DAIs to contract", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("5"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("30"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await expect(this.pool.connect(user).buyNFT(4)).to.be.revertedWith("ERC20: transfer amount exceeds balance");
        });

        it("Should revert semis when miss approve contract", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await expect(this.pool.connect(user).buyNFT(4)).to.be.revertedWith("ERC20: insufficient allowance");
        });

        it("Should revert semis when cant transfer DAIs to contract", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("5"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await expect(this.pool.connect(user).buyNFT(4)).to.be.revertedWith("ERC20: transfer amount exceeds balance");
        });

        it("Should revert final when miss approve contract", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await expect(this.pool.connect(user).buyNFT(4)).to.be.revertedWith("ERC20: insufficient allowance");
        });

        it("Should revert final when cant transfer DAIs to contract", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("5"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await expect(this.pool.connect(user).buyNFT(4)).to.be.revertedWith("ERC20: transfer amount exceeds balance");
        });
    });
});



describe("devTeamReceiveFunds", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const TicketsCollection = await ethers.getContractFactory("TicketsCollection", owner);
        this.nftContract = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi",
            "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract2 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract3 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract4 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address, 1657594799, 1660618799, 1663037999, 1667617199);

        await this.nftContract.setPool(this.pool.address);
        await this.nftContract2.setPool(this.pool.address);
        await this.nftContract3.setPool(this.pool.address);
        await this.nftContract4.setPool(this.pool.address);

        await this.stablecoin.mint(ethers.utils.parseEther("1000"));
    });

    describe("devTeamReceiveFunds() Function", function () {
        it("Should revert when try to receive founds in eighths", async function () {
            await expect(this.pool.devTeamReceiveFunds()).to.be.revertedWith("Is eighths!!");
        });

        it("Should receive 10% of an nft in eighths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("110"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));

            const oldBal = await this.stablecoin.balanceOf(owner.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.devTeamReceiveFunds();

            const newBal = await this.stablecoin.balanceOf(owner.address);
            const expectedBal = oldBal.add(ethers.utils.parseEther("1"));
            expect(expectedBal).to.eq(newBal);
        });

        it("Should receive 10% of impar nfts in eighths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("10"));

            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("10"));

            const oldBal = await this.stablecoin.balanceOf(owner.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(2);

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.devTeamReceiveFunds();

            const newBal = await this.stablecoin.balanceOf(owner.address);
            const expectedBal = oldBal.add(ethers.utils.parseEther("3"));
            expect(expectedBal).to.eq(newBal);
        });

        it("Should receive 10% of semifinal nfts", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));

            const oldBal = await this.stablecoin.balanceOf(owner.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.devTeamReceiveFunds();

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.devTeamReceiveFunds();

            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.connect(user).buyNFT(0);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);
            await this.pool.devTeamReceiveFunds();

            const newBal = await this.stablecoin.balanceOf(owner.address);
            const expectedBal = oldBal.add(ethers.utils.parseEther("5"));
            expect(expectedBal).to.eq(newBal);
        });

        it("Should receive 10% of FINAL nfts", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("70"));

            const oldBal = await this.stablecoin.balanceOf(owner.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.connect(user).buyNFT(0);
            await this.pool.connect(user_2).buyNFT(1);

            await this.pool.endVotingTime();
            await this.oracleMock.setFinalRoundResult(true);
            await this.pool.updateFinalRoundWinners();

            await this.pool.devTeamReceiveFunds();
            await this.pool.devTeamReceiveFunds();
            await this.pool.devTeamReceiveFunds();
            await this.pool.devTeamReceiveFunds();

            const newBal = await this.stablecoin.balanceOf(owner.address);
            const expectedBal = oldBal.add(ethers.utils.parseEther("14"));
            expect(expectedBal).to.eq(newBal);
        });
    });
});



describe("vote", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const TicketsCollection = await ethers.getContractFactory("TicketsCollection", owner);
        this.nftContract = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi",
            "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract2 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract3 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract4 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address, 1657594799, 1660618799, 1663037999, 1667617199);

        await this.nftContract.setPool(this.pool.address);
        await this.nftContract2.setPool(this.pool.address);
        await this.nftContract3.setPool(this.pool.address);
        await this.nftContract4.setPool(this.pool.address);

        await this.stablecoin.mint(ethers.utils.parseEther("1000"));
    });

    describe("eighthsRoundVote() Function", function () {
        it("Should receive 90% of the pool one winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("10"));

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).eighthsRoundVote(1, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_2).eighthsRoundVote(2, [false, false, true, false, true, true, false, true])
            await this.pool.connect(user_3).eighthsRoundVote(3, [false, false, true, false, true, true, false, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            const amountNftWinner = await this.pool.amountToClaim(1, 1);
            const amountNfUser2 = await this.pool.amountToClaim(1, 2);
            const amountNftUser3 = await this.pool.amountToClaim(1, 3);

            expect(ethers.utils.parseEther("27")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("0")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("0")).to.eq(amountNftUser3);
        });

        it("Should receive 90% of the pool two winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("10"));

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).eighthsRoundVote(1, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_2).eighthsRoundVote(2, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_3).eighthsRoundVote(3, [false, false, true, false, true, true, false, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            const amountNftWinner = await this.pool.amountToClaim(1, 1);
            const amountNfUser2 = await this.pool.amountToClaim(1, 2);
            const amountNftUser3 = await this.pool.amountToClaim(1, 3);

            expect(ethers.utils.parseEther("13.5")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("13.5")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("0")).to.eq(amountNftUser3);
        });

        it("Should receive 90% of the pool all players winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("10"));

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).eighthsRoundVote(1, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_2).eighthsRoundVote(2, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_3).eighthsRoundVote(3, [true, true, true, true, false, true, true, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            const amountNftWinner = await this.pool.amountToClaim(1, 1);
            const amountNfUser2 = await this.pool.amountToClaim(1, 2);
            const amountNftUser3 = await this.pool.amountToClaim(1, 3);

            expect(ethers.utils.parseEther("9")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("9")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("9")).to.eq(amountNftUser3);
        });

        it("Should revert when try to vote with a nft that you are not the owner in eighths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.pool.connect(user).buyNFT(1);

            await expect(this.pool.connect(user).eighthsRoundVote(2, [true, true, true, true, false, true, true, true]))
                .to.be.revertedWith("msg.sender does not own this nft");
        });

        it("Should revert when trying to vote with a vote that does not comply with the structure in eighths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.pool.connect(user).buyNFT(1);

            await expect(this.pool.connect(user).eighthsRoundVote(1, [true, true, true, false, true, true, true])) //seven games
                .to.be.revertedWith("does not meet the characteristics of this vote");
        });

        it("Should revert when you already voted in eighths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.pool.connect(user).buyNFT(1);

            await this.pool.connect(user).eighthsRoundVote(1, [true, true, true, true, false, true, true, true])

            await expect(this.pool.connect(user).eighthsRoundVote(1, [true, false, false, true, false, true, true, true]))
                .to.be.revertedWith("you already voted");
        });
    });

    describe("fourthRoundVote() Function", function () {
        it("Should receive 90% of the fourth pool one winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("30"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("30"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("30"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).fourthRoundVote(1, [true, true, false, true])
            await this.pool.connect(user_2).fourthRoundVote(2, [false, false, true, false])
            await this.pool.connect(user_3).fourthRoundVote(3, [true, false, true, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            const amountNftWinner = await this.pool.amountToClaim(2, 1);
            const amountNfUser2 = await this.pool.amountToClaim(2, 2);
            const amountNftUser3 = await this.pool.amountToClaim(2, 3);

            expect(ethers.utils.parseEther("81")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("0")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("0")).to.eq(amountNftUser3);
        });

        it("Should receive 90% of the fourth pool two winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("30"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("30"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("30"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).fourthRoundVote(1, [true, true, false, true])
            await this.pool.connect(user_2).fourthRoundVote(2, [false, false, false, false])
            await this.pool.connect(user_3).fourthRoundVote(3, [true, true, true, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            const amountNftWinner = await this.pool.amountToClaim(2, 1);
            const amountNfUser2 = await this.pool.amountToClaim(2, 2);
            const amountNftUser3 = await this.pool.amountToClaim(2, 3);

            expect(ethers.utils.parseEther("40.5")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("0")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("40.5")).to.eq(amountNftUser3);
        });

        it("Should receive 90% of the fourth pool all players winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("30"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("30"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("30"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).fourthRoundVote(1, [true, true, false, true])
            await this.pool.connect(user_2).fourthRoundVote(2, [false, true, false, true])
            await this.pool.connect(user_3).fourthRoundVote(3, [true, true, true, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            const amountNftWinner = await this.pool.amountToClaim(2, 1);
            const amountNfUser2 = await this.pool.amountToClaim(2, 2);
            const amountNftUser3 = await this.pool.amountToClaim(2, 3);

            expect(ethers.utils.parseEther("27")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("27")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("27")).to.eq(amountNftUser3);
        });

        it("Should revert when try to vote with a nft that you are not the owner in fourth", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("30"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);

            await expect(this.pool.connect(user).fourthRoundVote(2, [true, true, true, true, false, true, true, true]))
                .to.be.revertedWith("msg.sender does not own this nft");
        });

        it("Should revert when trying to vote with a vote that does not comply with the structure in fourth", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("30"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);

            await expect(this.pool.connect(user).fourthRoundVote(1, [true, true, true])) //seven games
                .to.be.revertedWith("does not meet the characteristics of this vote");
        });

        it("Should revert when you already voted in fourth", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("30"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("30"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);

            await this.pool.connect(user).fourthRoundVote(1, [true, true, true, true])

            await expect(this.pool.connect(user).fourthRoundVote(1, [true, false, false, true]))
                .to.be.revertedWith("you already voted");
        });
    });

    describe("semifinalRoundVote() Function", function () {
        it("Should receive 90% of the semis pool one winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).semifinalRoundVote(1, [true, true])
            await this.pool.connect(user_2).semifinalRoundVote(2, [true, false])
            await this.pool.connect(user_3).semifinalRoundVote(3, [false, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            const amountNftWinner = await this.pool.amountToClaim(3, 1);
            const amountNfUser2 = await this.pool.amountToClaim(3, 2);
            const amountNftUser3 = await this.pool.amountToClaim(3, 3);

            expect(ethers.utils.parseEther("135")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("0")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("0")).to.eq(amountNftUser3);
        });

        it("Should receive 90% of the semis pool two winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).semifinalRoundVote(1, [true, true])
            await this.pool.connect(user_2).semifinalRoundVote(2, [true, false])
            await this.pool.connect(user_3).semifinalRoundVote(3, [true, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            const amountNftWinner = await this.pool.amountToClaim(3, 1);
            const amountNfUser2 = await this.pool.amountToClaim(3, 2);
            const amountNftUser3 = await this.pool.amountToClaim(3, 3);

            expect(ethers.utils.parseEther("67.5")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("0")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("67.5")).to.eq(amountNftUser3);
        });

        it("Should receive 90% of the semis pool all players winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).semifinalRoundVote(1, [true, true])
            await this.pool.connect(user_2).semifinalRoundVote(2, [true, true])
            await this.pool.connect(user_3).semifinalRoundVote(3, [true, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            const amountNftWinner = await this.pool.amountToClaim(3, 1);
            const amountNfUser2 = await this.pool.amountToClaim(3, 2);
            const amountNftUser3 = await this.pool.amountToClaim(3, 3);

            expect(ethers.utils.parseEther("45")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("45")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("45")).to.eq(amountNftUser3);
        });

        it("Should revert when try to vote with a nft that you are not the owner in semis", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);

            await expect(this.pool.connect(user).semifinalRoundVote(2, [true, true]))
                .to.be.revertedWith("msg.sender does not own this nft");
        });

        it("Should revert when trying to vote with a vote that does not comply with the structure in semis", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);

            await expect(this.pool.connect(user).semifinalRoundVote(1, [true, true, true]))
                .to.be.revertedWith("does not meet the characteristics of this vote");
        });

        it("Should revert when you already voted in semis", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);

            await this.pool.connect(user).semifinalRoundVote(1, [true, true])

            await expect(this.pool.connect(user).semifinalRoundVote(1, [true, true]))
                .to.be.revertedWith("you already voted");
        });
    });

    describe("finalRoundVote() Function", function () {
        it("Should receive 90% of the final pool one winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(0);

            await this.pool.connect(user).finalRoundVote(1, true)
            await this.pool.connect(user_2).finalRoundVote(2, false)
            await this.pool.connect(user_3).finalRoundVote(3, false)

            await this.pool.endVotingTime();
            await this.oracleMock.setFinalRoundResult(true);
            await this.pool.updateFinalRoundWinners();

            const amountNftWinner = await this.pool.amountToClaim(4, 1);
            const amountNfUser2 = await this.pool.amountToClaim(4, 2);
            const amountNftUser3 = await this.pool.amountToClaim(4, 3);

            expect(ethers.utils.parseEther("189")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("0")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("0")).to.eq(amountNftUser3);
        });

        it("Should receive 90% of the final pool two winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(0);

            await this.pool.connect(user).finalRoundVote(1, true)
            await this.pool.connect(user_2).finalRoundVote(2, false)
            await this.pool.connect(user_3).finalRoundVote(3, true)

            await this.pool.endVotingTime();
            await this.oracleMock.setFinalRoundResult(true);
            await this.pool.updateFinalRoundWinners();

            const amountNftWinner = await this.pool.amountToClaim(4, 1);
            const amountNfUser2 = await this.pool.amountToClaim(4, 2);
            const amountNftUser3 = await this.pool.amountToClaim(4, 3);

            expect(ethers.utils.parseEther("94.5")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("0")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("94.5")).to.eq(amountNftUser3);
        });

        it("Should receive 90% of the final pool all players winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(0);

            await this.pool.connect(user).finalRoundVote(1, true)
            await this.pool.connect(user_2).finalRoundVote(2, true)
            await this.pool.connect(user_3).finalRoundVote(3, true)

            await this.pool.endVotingTime();
            await this.oracleMock.setFinalRoundResult(true);
            await this.pool.updateFinalRoundWinners();

            const amountNftWinner = await this.pool.amountToClaim(4, 1);
            const amountNfUser2 = await this.pool.amountToClaim(4, 2);
            const amountNftUser3 = await this.pool.amountToClaim(4, 3);

            expect(ethers.utils.parseEther("63")).to.eq(amountNftWinner);
            expect(ethers.utils.parseEther("63")).to.eq(amountNfUser2);
            expect(ethers.utils.parseEther("63")).to.eq(amountNftUser3);
        });

        it("Should revert when try to vote with a nft that you are not the owner in final", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);

            await expect(this.pool.connect(user).finalRoundVote(2, true))
                .to.be.revertedWith("msg.sender does not own this nft");
        });

        it("Should revert when you already voted in final", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);

            await this.pool.connect(user).finalRoundVote(1, true);

            await expect(this.pool.connect(user).finalRoundVote(1, false))
                .to.be.revertedWith("you already voted");
        });
    });
});

describe("claim", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const TicketsCollection = await ethers.getContractFactory("TicketsCollection", owner);
        this.nftContract = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi",
            "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract2 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract3 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract4 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address, 1657594799, 1660618799, 1663037999, 1667617199);

        await this.nftContract.setPool(this.pool.address);
        await this.nftContract2.setPool(this.pool.address);
        await this.nftContract3.setPool(this.pool.address);
        await this.nftContract4.setPool(this.pool.address);

        await this.stablecoin.mint(ethers.utils.parseEther("1000"));
    });

    describe("claim() Function", function () {
        it("Should revert when try to claim without rewards in eighths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("10"));

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).eighthsRoundVote(1, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_2).eighthsRoundVote(2, [false, false, true, false, true, true, false, true])
            await this.pool.connect(user_3).eighthsRoundVote(3, [false, false, false, false, false, false, false, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, true, true, true, true, true, true]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await expect(this.pool.connect(user_3).claim(1, 3)).to.be.revertedWith("There is nothing to claim");
        });

        it("Should revert when try to claim with a nft that are not yours in eighths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("10"));

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).eighthsRoundVote(1, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_2).eighthsRoundVote(2, [false, false, true, false, true, true, false, true])
            await this.pool.connect(user_3).eighthsRoundVote(3, [false, false, false, false, false, false, false, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, true, true, true, true, true, true]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await expect(this.pool.connect(user_3).claim(1, 1)).to.be.revertedWith("msg.sender does not own this nft");
        });

        it("Should revert when try to claim without rewards in fourths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, true, true, true, true, true, true]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).fourthRoundVote(1, [true, true, true, true])
            await this.pool.connect(user_2).fourthRoundVote(2, [false, false, false, false])
            await this.pool.connect(user_3).fourthRoundVote(3, [true, false, false, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, true, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await expect(this.pool.connect(user_3).claim(2, 3)).to.be.revertedWith("There is nothing to claim");
        });

        it("Should revert when try to claim with a nft that are not yours in fourths", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, true, true, true, true, true, true]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).fourthRoundVote(1, [true, true, true, true])
            await this.pool.connect(user_2).fourthRoundVote(2, [false, false, false, false])
            await this.pool.connect(user_3).fourthRoundVote(3, [true, false, false, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, true, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await expect(this.pool.connect(user_3).claim(2, 1)).to.be.revertedWith("msg.sender does not own this nft");
        });

        it("Should revert when try to claim without rewards in semis", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, true, true, true, true, true, true]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, true, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).semifinalRoundVote(1, [true, true])
            await this.pool.connect(user_2).semifinalRoundVote(2, [false, true])
            await this.pool.connect(user_3).semifinalRoundVote(3, [true, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await expect(this.pool.connect(user_3).claim(3, 3)).to.be.revertedWith("There is nothing to claim");
        });

        it("Should revert when try to claim with a nft that are not yours in semis", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, true, true, true, true, true, true]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, true, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).semifinalRoundVote(1, [true, true])
            await this.pool.connect(user_2).semifinalRoundVote(2, [false, true])
            await this.pool.connect(user_3).semifinalRoundVote(3, [true, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await expect(this.pool.connect(user_3).claim(3, 1)).to.be.revertedWith("msg.sender does not own this nft");
        });

        it("Should revert when try to claim without rewards in final", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, true, true, true, true, true, true]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, true, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(0);

            await this.pool.connect(user).finalRoundVote(1, true);
            await this.pool.connect(user_2).finalRoundVote(2, false);
            await this.pool.connect(user_3).finalRoundVote(3, false);

            await this.pool.endVotingTime();
            await this.oracleMock.setFinalRoundResult(true);
            await this.pool.updateFinalRoundWinners();

            await expect(this.pool.connect(user_3).claim(4, 3)).to.be.revertedWith("There is nothing to claim");
        });

        it("Should revert when try to claim with a nft that are not yours in final", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, true, true, true, true, true, true]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, true, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(0);

            await this.pool.connect(user).finalRoundVote(1, true);
            await this.pool.connect(user_2).finalRoundVote(2, false);
            await this.pool.connect(user_3).finalRoundVote(3, false);

            await this.pool.endVotingTime();
            await this.oracleMock.setFinalRoundResult(true);
            await this.pool.updateFinalRoundWinners();

            await expect(this.pool.connect(user_3).claim(4, 1)).to.be.revertedWith("msg.sender does not own this nft");
        });

        it("Should receive 90% of the eighths pool one winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("10"));

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).eighthsRoundVote(1, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_2).eighthsRoundVote(2, [false, false, true, false, true, true, false, true])
            await this.pool.connect(user_3).eighthsRoundVote(3, [false, false, true, false, true, true, false, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            const amountNftWinner = await this.pool.amountToClaim(1, 1);

            const oldBal = await this.stablecoin.balanceOf(user.address);
            await this.pool.connect(user).claim(1, 1);
            const newBal = await this.stablecoin.balanceOf(user.address);

            const expectedBal = oldBal.add(amountNftWinner);

            expect(expectedBal).to.eq(newBal);
        });

        it("Should receive 90% of the eighths pool three winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("10"));

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).eighthsRoundVote(1, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_2).eighthsRoundVote(2, [true, true, true, false, true, true, false, true])
            await this.pool.connect(user_3).eighthsRoundVote(3, [true, true, true, false, true, true, false, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            const amountNftWinner = await this.pool.amountToClaim(1, 1);

            const oldBal = await this.stablecoin.balanceOf(user.address);
            await this.pool.connect(user).claim(1, 1);
            const newBal = await this.stablecoin.balanceOf(user.address);
            const expectedBal = oldBal.add(amountNftWinner);

            expect(expectedBal).to.eq(newBal);
        });

        it("Should receive 90% of the fourth pool one winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).fourthRoundVote(1, [true, true, false, true])
            await this.pool.connect(user_2).fourthRoundVote(2, [false, false, false, false])
            await this.pool.connect(user_3).fourthRoundVote(3, [true, false, false, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            const amountNftWinner = await this.pool.amountToClaim(2, 1);

            const oldBal = await this.stablecoin.balanceOf(user.address);
            await this.pool.connect(user).claim(2, 1);
            const newBal = await this.stablecoin.balanceOf(user.address);
            const expectedBal = oldBal.add(amountNftWinner);

            expect(expectedBal).to.eq(newBal);
        });

        it("Should receive 90% of the fourth pool three winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).fourthRoundVote(1, [true, true, false, true])
            await this.pool.connect(user_2).fourthRoundVote(2, [true, true, false, true])
            await this.pool.connect(user_3).fourthRoundVote(3, [true, true, false, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            const amountNftWinner = await this.pool.amountToClaim(2, 1);
            const amountNftWinner2 = await this.pool.amountToClaim(2, 2);
            const amountNftWinner3 = await this.pool.amountToClaim(2, 3);

            const oldBal = await this.stablecoin.balanceOf(user.address);
            const oldBal2 = await this.stablecoin.balanceOf(user_2.address);
            const oldBal3 = await this.stablecoin.balanceOf(user_3.address);

            await this.pool.connect(user).claim(2, 1);
            await this.pool.connect(user_2).claim(2, 2);
            await this.pool.connect(user_3).claim(2, 3);

            const newBal = await this.stablecoin.balanceOf(user.address);
            const newBal2 = await this.stablecoin.balanceOf(user_2.address);
            const newBal3 = await this.stablecoin.balanceOf(user_3.address);

            const expectedBal = oldBal.add(amountNftWinner);
            const expectedBal2 = oldBal2.add(amountNftWinner2);
            const expectedBal3 = oldBal3.add(amountNftWinner3);

            expect(expectedBal).to.eq(newBal);
            expect(expectedBal2).to.eq(newBal2);
            expect(expectedBal3).to.eq(newBal3);
        });

        it("Should receive 90% of the semis pool one winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).semifinalRoundVote(1, [true, true])
            await this.pool.connect(user_2).semifinalRoundVote(2, [false, true])
            await this.pool.connect(user_3).semifinalRoundVote(3, [true, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            const amountNftWinner = await this.pool.amountToClaim(3, 1);

            const oldBal = await this.stablecoin.balanceOf(user.address);
            await this.pool.connect(user).claim(3, 1);
            const newBal = await this.stablecoin.balanceOf(user.address);
            const expectedBal = oldBal.add(amountNftWinner);

            expect(expectedBal).to.eq(newBal);
        });

        it("Should receive 90% of the semis pool three winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).semifinalRoundVote(1, [true, true])
            await this.pool.connect(user_2).semifinalRoundVote(2, [true, true])
            await this.pool.connect(user_3).semifinalRoundVote(3, [true, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            const amountNftWinner = await this.pool.amountToClaim(3, 1);
            const amountNftWinner2 = await this.pool.amountToClaim(3, 2);
            const amountNftWinner3 = await this.pool.amountToClaim(3, 3);

            const oldBal = await this.stablecoin.balanceOf(user.address);
            const oldBal2 = await this.stablecoin.balanceOf(user_2.address);
            const oldBal3 = await this.stablecoin.balanceOf(user_3.address);

            await this.pool.connect(user).claim(3, 1);
            await this.pool.connect(user_2).claim(3, 2);
            await this.pool.connect(user_3).claim(3, 3);

            const newBal = await this.stablecoin.balanceOf(user.address);
            const newBal2 = await this.stablecoin.balanceOf(user_2.address);
            const newBal3 = await this.stablecoin.balanceOf(user_3.address);

            const expectedBal = oldBal.add(amountNftWinner);
            const expectedBal2 = oldBal2.add(amountNftWinner2);
            const expectedBal3 = oldBal3.add(amountNftWinner3);

            expect(expectedBal).to.eq(newBal);
            expect(expectedBal2).to.eq(newBal2);
            expect(expectedBal3).to.eq(newBal3);
        });

        it("Should receive 90% of the final pool one winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(0);

            await this.pool.connect(user).finalRoundVote(1, true)
            await this.pool.connect(user_2).finalRoundVote(2, false)
            await this.pool.connect(user_3).finalRoundVote(3, false)

            await this.pool.endVotingTime();
            await this.oracleMock.setFinalRoundResult(true);
            await this.pool.updateFinalRoundWinners();

            const amountNftWinner = await this.pool.amountToClaim(4, 1);

            const oldBal = await this.stablecoin.balanceOf(user.address);
            await this.pool.connect(user).claim(4, 1);
            const newBal = await this.stablecoin.balanceOf(user.address);
            const expectedBal = oldBal.add(amountNftWinner);

            expect(expectedBal).to.eq(newBal);
        });

        it("Should receive 90% of the final pool three winners", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([true, true, false, true, false, false, true, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, false, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(0);

            await this.pool.connect(user).finalRoundVote(1, true)
            await this.pool.connect(user_2).finalRoundVote(2, true)
            await this.pool.connect(user_3).finalRoundVote(3, true)

            await this.pool.endVotingTime();
            await this.oracleMock.setFinalRoundResult(true);
            await this.pool.updateFinalRoundWinners();

            const amountNftWinner = await this.pool.amountToClaim(4, 1);
            const amountNftWinner2 = await this.pool.amountToClaim(4, 2);
            const amountNftWinner3 = await this.pool.amountToClaim(4, 3);

            const oldBal = await this.stablecoin.balanceOf(user.address);
            const oldBal2 = await this.stablecoin.balanceOf(user_2.address);
            const oldBal3 = await this.stablecoin.balanceOf(user_3.address);

            await this.pool.connect(user).claim(4, 1);
            await this.pool.connect(user_2).claim(4, 2);
            await this.pool.connect(user_3).claim(4, 3);

            const newBal = await this.stablecoin.balanceOf(user.address);
            const newBal2 = await this.stablecoin.balanceOf(user_2.address);
            const newBal3 = await this.stablecoin.balanceOf(user_3.address);

            const expectedBal = oldBal.add(amountNftWinner);
            const expectedBal2 = oldBal2.add(amountNftWinner2);
            const expectedBal3 = oldBal3.add(amountNftWinner3);

            expect(expectedBal).to.eq(newBal);
            expect(expectedBal2).to.eq(newBal2);
            expect(expectedBal3).to.eq(newBal3);
        });
    });
});


describe("isNotWinnerInEighthsRound", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const TicketsCollection = await ethers.getContractFactory("TicketsCollection", owner);
        this.nftContract = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi",
            "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract2 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address, 1657594799, 1660618799, 1663037999, 1667617199);

        await this.nftContract.setPool(this.pool.address);
        await this.nftContract2.setPool(this.pool.address);

        await this.stablecoin.mint(ethers.utils.parseEther("1000"));
    });

    describe("amountToClaim() Function with non winners", function () {
        it("Should receive 90% of the eighths pool with non winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("10"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("10"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("10"));

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).eighthsRoundVote(1, [true, true, true, true, false, true, true, true])
            await this.pool.connect(user_2).eighthsRoundVote(2, [true, true, true, false, true, true, false, true])
            await this.pool.connect(user_3).eighthsRoundVote(3, [false, false, true, true, true, true, false, true])

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([false, false, false, false, false, false, false, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await ethers.provider.send("evm_setNextBlockTimestamp", [1657594999]);

            const amountNftNonWinner = ethers.utils.parseEther("9");

            const oldBal = await this.stablecoin.balanceOf(user.address);
            await this.pool.connect(user).claim(1, 1);
            const newBal = await this.stablecoin.balanceOf(user.address);
            const expectedBal = oldBal.add(amountNftNonWinner);

            expect(expectedBal).to.eq(newBal);
        });
    });
});

describe("isNotWinnerInFourthRound", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const TicketsCollection = await ethers.getContractFactory("TicketsCollection", owner);
        this.nftContract = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi",
            "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract2 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract3 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address, 1657594799, 1660618799, 1663037999, 1667617199);

        await this.nftContract.setPool(this.pool.address);
        await this.nftContract2.setPool(this.pool.address);
        await this.nftContract3.setPool(this.pool.address);

        await this.stablecoin.mint(ethers.utils.parseEther("1000"));
    });

    describe("amountToClaim() Function with non winners", function () {
        it("Should receive 90% of the fourth pool with non winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([false, false, false, false, false, false, false, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).fourthRoundVote(1, [false, false, false, true])
            await this.pool.connect(user_2).fourthRoundVote(2, [false, false, false, true])
            await this.pool.connect(user_3).fourthRoundVote(3, [false, false, false, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, true, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await ethers.provider.send("evm_setNextBlockTimestamp", [1660618999]);

            const amountNftNonWinner = ethers.utils.parseEther("27");

            const oldBal = await this.stablecoin.balanceOf(user.address);
            await this.pool.connect(user).claim(2, 1);
            const newBal = await this.stablecoin.balanceOf(user.address);
            const expectedBal = oldBal.add(amountNftNonWinner);

            expect(expectedBal).to.eq(newBal);
        });
    });
});

describe("isNotWinnerInSemifinalRound", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const TicketsCollection = await ethers.getContractFactory("TicketsCollection", owner);
        this.nftContract = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi",
            "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract2 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract3 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract4 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address, 1657594799, 1660618799, 1663037999, 1667617199);

        await this.nftContract.setPool(this.pool.address);
        await this.nftContract2.setPool(this.pool.address);
        await this.nftContract3.setPool(this.pool.address);
        await this.nftContract4.setPool(this.pool.address);

        await this.stablecoin.mint(ethers.utils.parseEther("1000"));
    });

    describe("amountToClaim() Function with non winners", function () {
        it("Should receive 90% of the semis pool with non winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("50"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("50"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("50"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([false, false, false, false, false, false, false, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, true, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(3);

            await this.pool.connect(user).semifinalRoundVote(1, [false, true])
            await this.pool.connect(user_2).semifinalRoundVote(2, [false, true])
            await this.pool.connect(user_3).semifinalRoundVote(3, [true, false])

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await ethers.provider.send("evm_setNextBlockTimestamp", [1663039999]);

            const amountNftNonWinner = ethers.utils.parseEther("45");

            const oldBal = await this.stablecoin.balanceOf(user.address);
            await this.pool.connect(user).claim(3, 1);
            const newBal = await this.stablecoin.balanceOf(user.address);
            const expectedBal = oldBal.add(amountNftNonWinner);

            expect(expectedBal).to.eq(newBal);
        });
    });
});

describe("isNotWinnerInFinalRound", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const TicketsCollection = await ethers.getContractFactory("TicketsCollection", owner);
        this.nftContract = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi",
            "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract2 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract3 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);
        this.nftContract4 = await TicketsCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address, 1657594799, 1660618799, 1663037999, 1667617199);

        await this.nftContract.setPool(this.pool.address);
        await this.nftContract2.setPool(this.pool.address);
        await this.nftContract3.setPool(this.pool.address);
        await this.nftContract4.setPool(this.pool.address);

        await this.stablecoin.mint(ethers.utils.parseEther("1000"));
    });

    describe("amountToClaim() Function with non winners", function () {
        it("Should receive 90% of the final pool with non winner", async function () {
            await this.stablecoin.transfer(user.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_2.address, ethers.utils.parseEther("70"));
            await this.stablecoin.transfer(user_3.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_2).approve(this.pool.address, ethers.utils.parseEther("70"));
            await this.stablecoin.connect(user_3).approve(this.pool.address, ethers.utils.parseEther("70"));

            await this.pool.endVotingTime();
            await this.oracleMock.setEighthsRoundResult([false, false, false, false, false, false, false, false]);
            await this.pool.updateEighthsRoundWinnersAndSetFourthRound(this.nftContract2.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setFourthRoundResult([true, true, true, true]);
            await this.pool.updateFourthRoundWinnersAndSetSemifinalRound(this.nftContract3.address);

            await this.pool.endVotingTime();
            await this.oracleMock.setSemifinalRoundResult([true, true]);
            await this.pool.updateSemifinalRoundWinnersAndSetFinal(this.nftContract4.address);

            await this.pool.connect(user).buyNFT(1);
            await this.pool.connect(user_2).buyNFT(0);
            await this.pool.connect(user_3).buyNFT(0);

            await this.pool.connect(user).finalRoundVote(1, false)
            await this.pool.connect(user_2).finalRoundVote(2, false)
            await this.pool.connect(user_3).finalRoundVote(3, false)

            await this.pool.endVotingTime();
            await this.oracleMock.setFinalRoundResult(true);
            await this.pool.updateFinalRoundWinners();

            await ethers.provider.send("evm_setNextBlockTimestamp", [1667618199]);

            const amountNftNonWinner = ethers.utils.parseEther("63");

            const oldBal = await this.stablecoin.balanceOf(user.address);
            await this.pool.connect(user).claim(4, 1);
            const newBal = await this.stablecoin.balanceOf(user.address);
            const expectedBal = oldBal.add(amountNftNonWinner);

            expect(expectedBal).to.eq(newBal);
        });
    });
});